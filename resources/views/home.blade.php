@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                </div>
            </div>
        </div>
    </div>

    <table class="table table-condensed">
        <thead>
        <tr>
            <th>№</th>
            <th>Категория</th>
            <th>Название</th>
            <th>Цена</th>
            <th>Теги</th>
            <th>Описание</th>
        </tr>
        </thead>
        <tbody>

        @foreach ($products as $product)
            <tr class="row-{{ $product->id }}" data-json='{"id":"{{ $product->id }}","category":"{{ $product->category->id }}","name":"{{ $product->name }}", "price":"{{ $product->price }}", "desc":"{{ $product->desc }}"}'>
                <td class="id">{{ $product->id }}</td>
                <td class="category">{{ $product->category->name }}</td>
                <td class="name" >{{ $product->name }}</td>
                <td class="price" >{{ $product->price }}</td>
                <td class="tags" >{{ $product->tags_name }}</td>
                <td class="desc" >{{ $product->desc }}</td>
                <td>
                    <button type="button">Редактировать</button>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">New message</h4>
            </div>
            <div class="modal-body">
                <form>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label for="category" class="control-label">Категория:</label>

                        <select class="form-control" id="category" name="category_id">

                            @foreach ($categories as  $key => $category)
                                <option value="{{ $key }}" >{{ $category }}</option>
                            @endforeach

                        </select>

                    </div>
                    <div class="form-group">
                        <label for="name" class="control-label">Название:</label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                    <div class="form-group">
                        <label for="price" class="control-label">Цена:</label>
                        <input type="text" class="form-control" id="price" name="price">
                    </div>
                    <div class="form-group">
                        <label for="desc" class="control-label">Описание:</label>
                        <textarea class="form-control" name="desc" id="desc"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Send message</button>
            </div>
        </div>
    </div>
</div>
@endsection
