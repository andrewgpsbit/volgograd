<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsTag extends Model
{

    protected $appends = [
        'tag_name',
    ];


    /**
     * @return BelongsTo|Builder
     */
    public function tag()
    {
        return $this->belongsTo('App\Tag');
    }

    public function getTagNameAttribute()
    {
        return $this->tag->name;
    }
}
