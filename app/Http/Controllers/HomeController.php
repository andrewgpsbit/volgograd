<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use Illuminate\Http\Response;
use League\Flysystem\Exception;
use Illuminate\Database\QueryException;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $userId = $request->user()->id;
        $categories = Category::get()->pluck('name', 'id');
        $products = Product::where('user_id', $userId)->get();

        return view('home', [
            'products' => $products,
            'categories' => $categories
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)// for php7 : Response
    {

        $aInputs = $request->input();

        $product = Product::find($aInputs['id']);

        $attributes = array_keys($product->getAttributes());

        $inputs = collect($aInputs);


        $filtered = $inputs->filter(function ($value, $key) use ($attributes){
            return (in_array($key, $attributes)) ?  $value : false;
        })->toArray();


        try {
            $product->update($filtered);
        }catch(QueryException $e)
        {
            return response('error', Response::HTTP_BAD_REQUEST);
        }

        return response('ok', Response::HTTP_OK);
    }
}
