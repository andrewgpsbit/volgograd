<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use DB;
use App\Jobs\Test;



/**
 * Create admin user.
 */
class Devel extends Command
{

    protected $signature = 'devel {id?}';
    protected $description = 'For test development';

    use DispatchesJobs;

    public function __construct($id = null)
    {
        parent::__construct();

        $this->id = $id;

    }

    public function handle()
    {
        $sId = $this->argument('id');

        $job = new Test($sId);
        $job->delay(intval($sId));
        $this->dispatch($job);
    }

}