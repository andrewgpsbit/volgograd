<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $appends = [
        'tags_name',
    ];

    protected $fillable = [
        'id',
        'category_id',
        'name',
        'price',
        'desc'
    ];


    public $rules = [
        'price' => 'float',
        'name' => 'required',
        'category_id' => 'required|integer',
    ];


    /**
     * @return BelongsTo|Builder
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    /**
     * @return HasMany|Builder
     */
    public function tags()
    {
        return $this->hasMany('App\ProductsTag');
    }

    public function getTagsNameAttribute()
    {
        $tags = $this->tags->transform(function($item){
            return $item->tag_name;
        })->toArray();

        if(!empty($tags)){
            return implode(', ', $tags);
        }
        return false;
    }
}
