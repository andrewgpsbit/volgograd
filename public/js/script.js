$(document).ready(function($){

    var modal = $('#myModal');
    var tableTr = $("table tr");
    data = {};

    tableTr.click(function(){

        that = $(this);
        sJson = that.closest('tr').attr('data-json')
        oJson = $.parseJSON( sJson  )

        modal.find('#id').val(oJson.id);
        modal.find('#category').val(oJson.category);
        modal.find('#name').val(oJson.name);
        modal.find('#price').val(oJson.price);
        modal.find('#desc').val(oJson.desc);

        modal.modal('show')
    });


    modal.find('.btn-primary').click(function () {

        data = $('.modal-body form').serializeArray();

        $.ajax({
            type: "PUT",
            url: "/product/update",
            data: data,
            success: function(msg){
                listUpdate(data);
                modal.modal('hide')
            },
            error: function(msg){
                alert( "Что то пошло не так : " + msg.statusText );
            }
        });
    });


    function listUpdate(data) {

        modalUpdate = $('#myModal');
        productId = modalUpdate.find('#id').val();
        row= $('.row-'+productId);

        for(item in data){
            row.find('.'+data[item].name).text(data[item].value);
        }
    }
});