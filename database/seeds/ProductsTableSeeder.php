<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        DB::table('products')->insert([
            ['id' => 1, 'user_id' => 1, 'category_id' => 2,  'name' => 'Кросовки addidas', 'price' => 2100.00, 'desc' => 'Черные', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2, 'user_id' => 1, 'category_id' => 2,  'name' => 'Ботинки nike', 'price' => 3500.00, 'desc' => 'Белые', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 3, 'user_id' => 1, 'category_id' => 4,  'name' => 'Sony walkman', 'price' => 1500.00, 'desc' => 'Черный', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 4, 'user_id' => 1, 'category_id' => 5,  'name' => 'Nokia 6120', 'price' => 2500.00, 'desc' => 'Черный', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 5, 'user_id' => 1, 'category_id' => 7,  'name' => 'MacBook 200', 'price' => 200.00, 'desc' => 'Серый', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 6, 'user_id' => 1, 'category_id' => 8,  'name' => 'Asus 100', 'price' => 20.00, 'desc' => 'Черный', 'created_at' => $now, 'updated_at' => $now],
        ]);

        DB::table('products_tags')->insert([
            [ 'product_id' => 1, 'tag_id' => 1],
            [ 'product_id' => 2, 'tag_id' => 1],
            [ 'product_id' => 3, 'tag_id' => 2],
            [ 'product_id' => 4, 'tag_id' => 2],
            [ 'product_id' => 5, 'tag_id' => 3],
            [ 'product_id' => 5, 'tag_id' => 3],
        ]);
    }
}
