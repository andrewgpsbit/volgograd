<?php

use Illuminate\Database\Seeder;
use DB;
use Carbon\Carbon;


class AllTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        DB::table('categories')->insert([
            ['id' => 1, 'name' => 'Одежда', 'desc' => 'Разная одежда', 'parent_id' => null, 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2, 'name' => 'Обувь', 'desc' => 'Разная обувь', 'parent_id' => 1, 'created_at' => $now, 'updated_at' => $now],
            ['id' => 3, 'name' => 'Телефоны', 'desc' => 'Разные телефоны', 'parent_id' => null, 'created_at' => $now, 'updated_at' => $now],
            ['id' => 4, 'name' => 'Sony', 'desc' => 'Сони', 'parent_id' => 3, 'created_at' => $now, 'updated_at' => $now],
            ['id' => 5, 'name' => 'Nokia', 'desc' => 'Нокиа', 'parent_id' => 3, 'created_at' => $now, 'updated_at' => $now],
            ['id' => 6, 'name' => 'Ноутбуки', 'desc' => 'Разные ноутбуки', 'parent_id' => null, 'created_at' => $now, 'updated_at' => $now],
            ['id' => 7, 'name' => 'MacBook', 'desc' => 'Мак', 'parent_id' => 6, 'created_at' => $now, 'updated_at' => $now],
            ['id' => 8, 'name' => 'Asus', 'desc' => 'Асус', 'parent_id' => 6, 'created_at' => $now, 'updated_at' => $now],
        ]);


        DB::table('tags')->insert([
            ['id' => 1, 'name' => 'Одежда', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2, 'name' => 'Телефоны', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 3, 'name' => 'Ноутбуки', 'created_at' => $now, 'updated_at' => $now],
        ]);
    }
}
